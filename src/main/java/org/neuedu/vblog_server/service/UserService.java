package org.neuedu.vblog_server.service;

import org.neuedu.vblog_server.mapper.UserMapper;
import org.neuedu.vblog_server.model.Role;
import org.neuedu.vblog_server.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService implements UserDetailsService {
    @Autowired
    UserMapper userMapper;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userMapper.loadUserByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("没找到该用户");
        }
        List<Role> roles = userMapper.getRolesById(user.getId());
        user.setRoles(roles);
        return user;
    }
}
