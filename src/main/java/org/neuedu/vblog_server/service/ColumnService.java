package org.neuedu.vblog_server.service;

import org.neuedu.vblog_server.mapper.ColumnMapper;
import org.neuedu.vblog_server.model.Column;
import org.neuedu.vblog_server.model.PageInfoUtils;
import org.neuedu.vblog_server.model.RespBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ColumnService {
    @Autowired
    ColumnMapper columnMapper;

    public PageInfoUtils getAllColumn() {
        PageInfoUtils infos = new PageInfoUtils();
        infos.setCode(0);
        infos.setMsg("查询成功");
        List<Column> allColumn = columnMapper.getAllColumn();
        infos.setCount(allColumn.size());
        infos.setData(allColumn);
        return infos;
    }

    public RespBean addColumn(Column column) {
        int i = columnMapper.addColumn(column);
        if (i != 0) {
            return RespBean.ok("新增成功");
        } else {
            return RespBean.error("未知错误");
        }
    }

    public RespBean delColumn(Integer id) {
        int i = columnMapper.delColumn(id);
        if (i != 0) {
            return RespBean.ok("删除成功");
        } else {
            return RespBean.error("删除失败");
        }
    }

    public RespBean updateColumn(Column column) {
        int i = columnMapper.updateColumn(column);
        if (i != 0) {
            return RespBean.ok("修改成功");
        } else {
            return RespBean.error("修改失败");
        }
    }

    public RespBean delManyColumn(List<String> idList) {
        int i = columnMapper.delManyColumn(idList);
        if (i != 0) {
            return RespBean.ok("批量删除成功");
        }else{
            return RespBean.error("批量删除失败");
        }
    }
}
