package org.neuedu.vblog_server.controller;


import org.neuedu.vblog_server.model.Column;
import org.neuedu.vblog_server.model.PageInfoUtils;
import org.neuedu.vblog_server.model.RespBean;
import org.neuedu.vblog_server.service.ColumnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/column")
public class ColumnController {
    @Autowired
    ColumnService columnService;
    @GetMapping("/getAllColumn")
    @CrossOrigin
    public PageInfoUtils getAllColumn() {
        return columnService.getAllColumn();
    }

    @PostMapping("/")
    @CrossOrigin
    public RespBean addColumn(@RequestBody Column column) {
        return columnService.addColumn(column);
    }

    @DeleteMapping("/{id}")
    @CrossOrigin
    public RespBean delColumn(@PathVariable("id") Integer id) {
        return columnService.delColumn(id);
    }

    @PutMapping("/")
    @CrossOrigin
    public RespBean updateColumn(Column column) {
        return columnService.updateColumn(column);
    }

    @PostMapping("/delMany")
    @CrossOrigin
    public RespBean delManyColumn(@RequestParam(value="ids",defaultValue = "") List<String> idList) {
        if(idList == null || idList.size() == 0){
            return RespBean.error("批量删除至少需要一个参数");
        }
        return columnService.delManyColumn(idList);
    }
}
