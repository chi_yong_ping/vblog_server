package org.neuedu.vblog_server.controller;

import org.neuedu.vblog_server.model.RespBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {
    @GetMapping("/login")
    public RespBean login() {
        return RespBean.error("用户未登录，请先登录！");
    }
}
