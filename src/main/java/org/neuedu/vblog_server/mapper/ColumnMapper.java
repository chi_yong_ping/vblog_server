package org.neuedu.vblog_server.mapper;

import org.apache.ibatis.annotations.Param;
import org.neuedu.vblog_server.model.Column;

import java.util.List;

public interface ColumnMapper {
    List<Column> getAllColumn();

    int addColumn(Column column);

    int delColumn(Integer id);

    int updateColumn(Column column);

    int delManyColumn(@Param("ids") List<String> idList);
}
