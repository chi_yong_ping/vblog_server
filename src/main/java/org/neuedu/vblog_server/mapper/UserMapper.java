package org.neuedu.vblog_server.mapper;

import org.neuedu.vblog_server.model.Role;
import org.neuedu.vblog_server.model.User;

import java.util.List;

public interface UserMapper {
    User loadUserByUsername(String username);
    List<Role> getRolesById(Integer id);
}
