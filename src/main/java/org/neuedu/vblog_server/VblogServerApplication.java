package org.neuedu.vblog_server;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = "org.neuedu.vblog_server.mapper")
public class VblogServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(VblogServerApplication.class, args);
    }

}
